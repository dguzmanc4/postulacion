import java.util.Scanner;

class Fibonacci {
    public static void main(String[] args) {
        System.out.println(isFibonacci(100));
        System.out.println(isFibonacci(2));
        System.out.println(isFibonacci(4));
        System.out.println(isFibonacci(-100));
    }

    public static int getUserNumber() {
        System.out.println("Ingrese un numero entero");
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNextInt()) sc.next();
        return sc.nextInt();
    }

    public static boolean isFibonacci(int number) {
        if (number == 1 || number == 0) return true;
        if (number < 0) return false;

        int firstNumber = 0;
        int secondNumber = 1;
        int aux;

        while (secondNumber < number) {
            aux = firstNumber;
            firstNumber = secondNumber;
            secondNumber += aux;

            if (secondNumber == number) {
                return true;
            }
        }
        return false;
    }
}
