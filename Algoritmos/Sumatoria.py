
numbers = [ 1, 2, 5, 8, [ 2, 3, [ 1, 2 ] ] ]

def nested_sum(array):
    total = 0
    for i in array:
        if isinstance(i, list):
            total += nested_sum(i)
        else:
            total += i
    return total

print(nested_sum(numbers))
